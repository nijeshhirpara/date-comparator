<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Website CSS style -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    <!-- Website Font style -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{ URL::asset('css/jquery-ui-timepicker-addon.css') }}">

    <title>{{$pageTitle}}</title>
</head>
<body>
<div class="container">
    <div class="row main mt-5">
        <div class="col-xs-12 col-sm-5 col-sm-offset-3">
            <h5 class="text-center">Date Comparator</h5>
            <form class="" method="post" action="{{url('/')}}/">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="date1" class="cols-sm-2 control-label">Date From</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="date1" id="date1"  value="{{ Input::get('date1') }}" placeholder="Date from"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-clock-o fa" aria-hidden="true"></i></span>
                            <select class="form-control" name="timeZone1" id="timeZone1">
                                <option value="">Select Time Zone</option>
                                @foreach ($timeZone as $key => $node)
                                    <option value="{{ $node }}" {{ (Input::get('timeZone1') == $node)?'selected="selected"':''}}>{{ $node }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="date2" class="cols-sm-2 control-label">Date to</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="date2" id="date2" value="{{ Input::get('date2') }}"  placeholder="Date to"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-clock-o fa" aria-hidden="true"></i></span>
                            <select class="form-control" name="timeZone2" id="timeZone2">
                                <option value="">Select Time Zone</option>
                                @foreach ($timeZone as $key => $node)
                                    <option value="{{ $node }}" {{ (Input::get('timeZone2') == $node)?'selected="selected"':''}}>{{ $node }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="operation" class="cols-sm-2 control-label">Calculate</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-cog fa" aria-hidden="true"></i></span>
                            <select class="form-control" name="operation" id="operation">
                                <option value="days" {{ (Input::get('operation') == 'days')?'selected="selected"':''}}>Difference in days</option>
                                <option value="weekdays" {{ (Input::get('operation') == 'weekdays')?'selected="selected"':''}}>Number of weekdays</option>
                                <option value="weeks" {{ (Input::get('operation') == 'weeks')?'selected="selected"':''}}>Complete weeks</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="otherFormat" class="cols-sm-2 control-label">Select other format</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-cog fa" aria-hidden="true"></i></span>
                            <select class="form-control" name="otherFormat" id="otherFormat">
                                <option value="">Please select</option>
                                <option value="seconds" {{ (Input::get('otherFormat') == 'seconds')?'selected="selected"':''}}>Seconds</option>
                                <option value="minutes" {{ (Input::get('otherFormat') == 'minutes')?'selected="selected"':''}}>Minutes</option>
                                <option value="hours" {{ (Input::get('otherFormat') == 'hours')?'selected="selected"':''}}>Hours</option>
                                <option value="years" {{ (Input::get('otherFormat') == 'years')?'selected="selected"':''}}>Years</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group ">
                    <button type="submit" id="button" class="btn btn-primary btn-lg btn-block login-button">Process</button>
                </div>
            </form>

            @if(isset($res) && $res != '')
                        <h4 class="text-center  ">Result</h4>
                        <div class="cols-sm-10 text-center">
                                <b>{{$res}}</b>
                        </div>
            @endif
        </div>

    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
    $( function() {
        $( "#date1" ).datetimepicker();
        $( "#date2" ).datetimepicker();
    } );

</script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script src="{{ URL::asset('js/jquery-ui-timepicker-addon.js') }}"></script>
</body>
</html>