<?php

namespace App\Http\Controllers;


use App\Http\Requests\checkValidDateRequest;
use App\Services\TimeComparatorService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DateTimeZone;

class TimeController extends Controller
{

    public function __construct(TimeComparatorService $timeComparatorService)
    {
        $this->timeComparatorService = $timeComparatorService;
    }

    public function index()
    {
        $headData = array('pageTitle' => 'Home - Time comparator');

        $timeZoneList = $this->getTimeZoneList();

        return view('time',array_merge($headData,$timeZoneList));
    }

    private function getTimeZoneList(){
        return array("timeZone"=>DateTimeZone::listIdentifiers(DateTimeZone::AUSTRALIA | DateTimeZone::AMERICA));
    }

    public function getResult(checkValidDateRequest $request)
    {
        $headData = array('pageTitle' => 'Result - Time comparator');

        $timeZoneList = $this->getTimeZoneList();

        $res = "";

        $fromDate = $this->timeComparatorService->createDate(Input::get('date1'),'m/d/Y H:i',Input::get('timeZone1'));

        $toDate = $this->timeComparatorService->createDate(Input::get('date2'),'m/d/Y H:i',Input::get('timeZone2'));

        switch (Input::get('operation')){
            case 'days':        $res = $this->timeComparatorService->getDaysBetweenDates($fromDate, $toDate, Input::get('otherFormat'));
                                break;

            case 'weekdays':    $res = $this->timeComparatorService->getWeekDaysBetweenDates($fromDate, $toDate, Input::get('otherFormat'));
                                break;

            case 'weeks':       $res = $this->timeComparatorService->getCompleteWeeksBetweenDates($fromDate, $toDate, Input::get('otherFormat'));
                                break;
        }

        return view('time',array_merge($headData,$timeZoneList))->with(compact('res'));
    }

}
