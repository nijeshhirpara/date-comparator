<?php

namespace App\Services;
use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: UserPC
 * Date: 29/05/2017
 * Time: 9:36 PM
 */
class TimeComparatorService
{
    public function createDate($date, $format = null, $timeZone = null){

        $format = (is_null($format))?'m/d/Y':$format;

        if(isset($timeZone)) {
            $date = Carbon::createFromFormat($format, $date, $timeZone);
        }else{
            $date = Carbon::createFromFormat($format, $date);
        }

        $date->setTimezone('UTC');

        return $date;
    }
    public function getDaysBetweenDates($date1, $date2, $otherFormat =  null)
    {
        $res = "";

        $diff = $date1->diffInDays($date2);

        if($diff > 0){
            $res = ($diff>1)?"{$diff} days":"{$diff} day";

            if(isset($otherFormat)){
                $now  = Carbon::now();
                $out = $this->getOtherFormats($now,$now->copy()->addDays($diff),$otherFormat);
                $res.=" OR {$out}";
            }
            return $res;
        }

        return "There isn't any difference between given dates";
    }

    private function getOtherFormats($now,$gap,$otherFormat = null){
        $res = "";

        switch ($otherFormat){
            case "hours":   $diff = $now->diffInHours($gap);
                            $res = ($diff>1)?"{$diff} hours":"{$diff} hour";
                            break;

            case "minutes": $diff = $now->diffInMinutes($gap);
                            $res = ($diff>1)?"{$diff} minutes":"{$diff} minutes";
                            break;

            case "seconds": $diff = $now->diffInSeconds($gap);
                            $res = ($diff>1)?"{$diff} seconds":"{$diff} second";
                            break;

            case "years":   $diff = $now->diffInYears($gap);
                            $res = ($diff>1)?"{$diff} years":"{$diff} year";
                            break;
        }
        return $res;
    }

    public function getWeekDaysBetweenDates($date1, $date2, $otherFormat){

        $diff = $date1->diffInWeekdays($date2);

        if($diff > 0){
            $res = ($diff>1)?"{$diff} days":"{$diff} day";
            if(isset($otherFormat)){
                $now  = Carbon::now();
                $out = $this->getOtherFormats($now,$now->copy()->addDays($diff),$otherFormat);
                $res.=" OR {$out}";
            }
            return $res;
        }

        return "There isn't any difference between given dates";
    }

    public function getCompleteWeeksBetweenDates($date1, $date2, $otherFormat){

        $diff = $date1->diffInWeeks($date2);

        if($diff > 0){
            $res = ($diff>1)?"{$diff} weeks":"{$diff} week";
            if(isset($otherFormat)){
                $now  = Carbon::now();
                $now = $this->getOtherFormats($now,$now->copy()->addWeeks($diff),$otherFormat);
                $res.=" OR {$now}";
            }
            return $res;
        }

        return "There isn't any complete week/s between given dates";
    }

}