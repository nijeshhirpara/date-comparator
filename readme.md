## Date Comparator

This application has been built in Laravel. It basically performs following date operations.

1. Calculate the number of days between two datetime parameters.

2. Find out the number of weekdays between two datetime parameters.

3. Find out the number of complete weeks between two datetime parameters.

4. Accept a third parameter to convert the result of (1, 2 or 3) into one of seconds, minutes, hours, years.

5. Allow the specification of a timezone for comparison of input parameters from different timezones.

My aim is to demonstrate date comparison using Carbon class. Bootstrap framework has been used for GUI.